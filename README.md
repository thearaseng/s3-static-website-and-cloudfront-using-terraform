## CloudFront Using Terraform and Gitlab
You can find the complete step-by-step guide here in my blog post [https://thearaseng.com/blogs/s3-static-website-and-cloudfront-using-terraform](https://thearaseng.com/blogs/s3-static-website-and-cloudfront-using-terraform)

![Alt text](aws-s3-terraform-gitlab.png?raw=true)
